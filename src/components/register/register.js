import React from 'react';

class Register extends React.Component {

    constructor(props){
        super(props);
        this.state = {
         username : "",
         password: "",
         confirm:""
        }
      }

    render() {
        return (
            <div className="container">
                <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <form>
                        <legend>Register</legend>
                        <div className="form-group">
                            <label >Username</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                placeholder="Input field" 
                                value = {this.state.value} 
                                onChange={(event)=>{this.setState({username:event.target.value})}}/>
                        </div>
                        <div className="form-group">
                            <label >Password</label>
                            <input 
                                type="password" 
                                className="form-control" 
                                placeholder="Input field" 
                                value = {this.state.value} 
                                onChange={(event)=>{this.setState({password:event.target.value})}}/>
                        </div>
                        <div className="form-group">
                            <label >Confirm password</label>
                            <input 
                                type="password" 
                                className="form-control"  
                                placeholder="Input field" 
                                value = {this.state.value} 
                                onChange={(event)=>{this.setState({confirm:event.target.value})}}/>
                        </div>
                        <button className="btn btn-primary" onClick={()=>this.props.onclickRegisterBtn(this.state.username, this.state.password,this.state.confirm)}>Register</button>
                        <div className="form-group">
                            <a href="#" onClick={this.props.onclickLinkLogin}>Back to login page</a>
                        </div>
                    </form>
                </div>
            </div>

        );
    }
}

export { Register };
