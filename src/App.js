import React from 'react';
import axios from 'axios';
import './App.css';
import {Login } from './components/login/login';
import {Register } from './components/register/register';



class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      login : true,
      users : [],
      account: []
    }

    this.onclickLoginBtn = this.onclickLoginBtn.bind(this);
    this.onclickLinkRegister = this.onclickLinkRegister.bind(this);
    this.onclickRegisterBtn = this.onclickRegisterBtn.bind(this);
    this.onclickLinkLogin = this.onclickLinkLogin.bind(this);
  }


  onclickLoginBtn(username, password){
    axios .get("http://172.16.2.79:1494/api/accounts") 
      .then(response => {
          this.setState({ users: response.data });
          var count = 0;
          this.state.users.find(function(element){
            if(element.name === username && element.password === password) { alert("Welcome "+ element.name); return element;}
            count ++; 
          });
          if(count === this.state.users.length){alert("Username and password not right");}

      	})
    .catch(err => console.log(err));
  }

  onclickLinkRegister(){
    this.setState(
      {login:false,
        users: []}
    )
  }

  onclickRegisterBtn(username, password, confirm){
    if(password === confirm){
      const user = {
        name: username,
        password: password
      };
      axios.post("http://172.16.2.79:1494/api/accounts", {
        name : user.name,
        password : user.password
      })
      .then(response => {
        // alert(response);
        // alert(response.data);
        // alert("Account successfully created")
      })
      .catch(error => {
        console.log(error.response)
    });
    alert("Account successfully created")
    }else alert("Incorrect confirm password")
  }

  onclickLinkLogin(){
    this.setState(
      {login:true}
    )
  }

  

  render(){
    if(this.state.login) {
      return (
      <Login 
        onclickLoginBtn = {this.onclickLoginBtn} 
        onclickLinkRegister = {this.onclickLinkRegister}/>
        );
        
      }
    return (
      <Register
        onclickRegisterBtn = {this.onclickRegisterBtn} 
        onclickLinkLogin = {this.onclickLinkLogin}/>
        );
  }
}

export default App;