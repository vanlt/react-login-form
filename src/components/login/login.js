import React from 'react';

class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
         username : "",
         password: ""
        }
      }
    render(){
        return (
            <div className="container">
            <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <form>
                <legend>Login</legend>
                <div className="form-group">
                    <label >Username</label>
                    <input 
                        type="text" 
                        className="form-control"  
                        placeholder="Input field" 
                        value = {this.state.value} 
                        onChange={(event)=>{this.setState({username:event.target.value})}}/>
                </div>
                <div className="form-group">
                    <label >Password</label>
                    <input 
                        type="password" 
                        className="form-control" 
                        placeholder="Input field" 
                        value = {this.state.value} 
                        onChange={(event)=>{this.setState({password:event.target.value})}}/>
                </div>
                <button className="btn btn-primary" onClick={()=>this.props.onclickLoginBtn(this.state.username, this.state.password)}>Login</button>
                <div className="form-group">
                    <a href="#" onClick = {this.props.onclickLinkRegister}>Register Here</a>
                </div>
                
            </form>
            </div>
            </div>
          );
    }
}

export {Login};
